// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('prototype', ['ionic', 'prototype.controllers', 'prototype.services', 'backand'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})


.config(function($stateProvider, $urlRouterProvider, BackandProvider) {
    
    BackandProvider.setAppName('alarmprototype');
    BackandProvider.setAnonymousToken('b7330e25-69c6-4060-921a-caa2373c1a6d');
    
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
    $stateProvider

  // These are the states for the app and caching has been turned off
  // in order to make sure that the controllers are 'loaded' everytime
  // a state is loaded.
        .state('home', {
        cache: false,
        url: '/home',
        templateUrl: 'templates/home.html',
        controller: 'PatientsCtrl'
        })
        .state('specific', {
        cache: false,
        url: '/specific',
        templateUrl: 'templates/patient-info.html',
        controller: 'SpecificCtrl'
        })
  // default state
    $urlRouterProvider.otherwise('/home');
});



