
//To find documentation regarding this use of OPEN EPIC go to https://open.epic.com/Interface/FHIR and look under the "Oberservation" tab to explore the spec. You need an account with them to be able to browse the spec
/*
var test;
var patientID = 'Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB';
var request =  $http({
	method: 'GET',
  url: 'https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Observation?patient=' +patientID+'&code=8867-4,8310-5&date=20160419'
}).then(function successCallback(response) {
  test = response;
  return response;
},
	function errorCallback(response) {
  });
  
  request.then(function (data) {
  console.log(test);
  
  });
}]);
*/


// New module for the services this app will be using
angular.module('prototype.services', [])

// New factory that uses the $filter service
.factory('Patients', ['$filter', '$http', 'Backand', '$q', '$interval', function($filter, $http, Backand, $q, $interval) {


// These two variables are just for QOL and store the strings used to connect with Backand
var baseUrl = '/1/objects/';
var objectName =  'Patient/';

// Boolean used to determine if the front end array has been loaded on startup
var loaded = false;

//Variable holding the ID of the patient that is "active". Used to display the information for the current
//Patient.
var currentID = 3;

//Auto increment to deal with ID's
var incrementID = 0;    

//Variable holding the array index of the current patient, found using the currentID
var currentPosition;

//Array holding the patient objects
var patients = [];

//This was used during testing. Use when disconnecting from Backand
/*var patients = [{
    id: 0,
    name: 'Alexander Hamilton',
    heartUpper: 90,
    heartLower: 60
}];*/

// THis function uses Backands API and retrieves the specific url for the apps server
function getUrl() {
    return Backand.getApiUrl() + baseUrl + objectName;
}
    
// This function just calls the getUrl() function and returns a promise used to interact with the server 
function getPatients() {
    return $http.get(getUrl());
}
    
// Test function that just prints patients
function testPrint() {
    console.log(patients);
}
    

        
// Function that the filter() function refers to in order to filter through array
function matchesID(patient) {
    return patient.id == currentID;
}
   
// Uses the getPatients() method in order to update the backend array from Backand
function getAllPatients() {
 getPatients()
        .then(function (result) {
            console.log("--Updating patients array in services-- Source: getAllPatients()")
            patients =  result.data.data;
            testPrint();
        });   
}
    
function monitorPatient() {
 getPatients()
        .then(function (result) {
            console.log("--Updating patients array in services-- Source: monitorPatient()")
            patients =  result.data.data;
            currentPosition = patients.map(function(e) {return e.id;}).indexOf(currentID);
            if(patients[currentPosition].Heart_Rate > patients[currentPosition].Heart_Rate_Upper){
                alert("--ALARM TRIGGERED BY EXCEEDING UPPER LIMIT-- Value: " + patients[currentPosition].Heart_Rate);
            }
     
            else if(patients[currentPosition].Heart_Rate < patients[currentPosition].Heart_Rate_Lower){
                console.log("--ALARM TRIGGERED BY EXCEEDING LOWER LIMIT-- Value: " + patients[currentPosition].Heart_Rate);   
            }
            else 
            console.log("--ALARM NOT TRIGGERED--");
        });   
}
   
function genRanHeartRate(min, max) {
    return Math.floor(Math.random() * (max-min + 1)) + min;
}

var timer

function stop() {
    if(angular.isDefined(timer)) {
        $interval.cancel(timer);
        timer = undefined;
    }
}
    
var count = 0;    
timer = $interval(function() {
    var ran = genRanHeartRate(40, 120);
    console.log("New heart rate: " + ran);
    //console.log(ran);
    count++;
    //console.log("count: " + count);
     $http({
            method:'PUT',
            url: getUrl() + '/' + currentID,
            data: {
                Heart_Rate: ran
            } 
        }).then(function(response) {
           console.log("--Finished updating heart rate. Calling monitorPatient--");
            monitorPatient();
        });    
    
    if(count == 5) {
        stop();
    }
}, 1000);
    
// functions for the factory
return {
    
    // Allows other classes to call these functions
    getAllPatients: getAllPatients,
    
    testPrint: testPrint,
    
    getPatients: getPatients,
    
    //addPatient: function(patient) {
    //    return $http.post(getUrl(), patient)
    //},
    
    // Getter for the array holding the patients
    all:function() {
        return patients;
    },
    
    // Add function that takes in a patient and pushes it to the backand server
    add: function(patient) {
        
        // Test statements for using local test array 
        /*patients.push( {
            id: ++incrementID,
            name: patient.name,
            heartUpper: patient.heartUpper,
            heartLower: patient.heartLower
        })*/
        
        console.log("--Adding patient to patient array-- Source: add()");
        
        // http request. Using POST to create patient. After we have POSTED the data to the server, run the getAllPatients() function in order to update the patients array 
        $http({
            method: 'POST',
            url: getUrl(),
            data: {
                Name: patient.Name,
                Heart_Rate_Upper: patient.Heart_Rate_Upper,
                Heart_Rate_Lower: patient.Heart_Rate_Lower
            }
        }).then(function (response) {
            console.log("--Calling getAllPatients() in add--");
            getAllPatients();
        });
    },
    
    // This function sets the current ID to that of the 'active patient'
    setID: function(givenID) {
        console.log("Setting currentID in service to" + givenID);
        currentID = givenID;

    },
    
    // Getter
    getID: function() {
        return currentID;    
    },
    
    // Getter for the active patient. Two ways to do it, one with the $filter service and the other with 
    // The filter() function
    getPatient: function() {
        
        //var patient = patients.filter(matchesID);
        //return patient[0];
        var currentPatient = $filter('filter')(patients, {id: currentID}, true);
        return currentPatient[0];
    },
    
    // Getter
    getLoaded: function() {
        return loaded;
    },
    
    // Setter
    setLoaded: function() {
        loaded  = true;
    },
    
    // This is the main update function that deals with updating the paramters for the current patient.
    // We use the PUT method in the http request to make the update. We have to provide the ID of the
    // patient that we want to update, thankfully we already have this from the setID() method.
    // Like creating a new patient, we update the patients array after we have updated the patients info
    update: function(updatedHeartUpper, updatedHeartLower) {
        
        console.log("--Updating current patient limits-- Source: update()")
        $http({
            method:'PUT',
            url: getUrl() + '/' + currentID,
            data: {
                Heart_Rate_Upper: updatedHeartUpper,
                Heart_Rate_Lower: updatedHeartLower
            } 
        }).then(function(response) {
           console.log("--Calling getAllPatients() in update()--");
            getAllPatients();
        });
        
        
        // This updates the current position variable before we start changing any of the parameters.
        // the map() function here returns the position of the object with an ID that matches "currentID"
        //currentPosition = patients.map(function(e) {return e.id;}).indexOf(currentID);
        
        // Here we only try to update the currentpatients parameters when the new value is not null
        /*
        if (updatedHeartUpper != null) {
            patients[currentPosition].heartUpper = updatedHeartUpper;
        }
        
        if (updatedHeartLower != null) {
            patients[currentPosition].heartLower = updatedHeartLower;
        }*/
        
        /*switch(source) {
            case 1:
                console.log(patients[currentPosition].name + "'s upper limit was " + patients[currentPosition].heartUpper);
                patients[currentPosition].heartUpper = updated;
                console.log("Updated " + patients[currentPosition].name+ "'s upper limit to " + updated );
                break;
            case 2:
                console.log(patients[currentPosition].name + "'s upper limit was " + patients[currentPosition].heartLower);
                patients[currentPosition].heartLower = updated;
                console.log("Updated " + patients[currentPosition].name+ "'s lower limit to " + updated );
                break;
        }*/
    },
    
    //Deletes the current patient
    delete: function() {
        
        // Delete function using the DELETE method for httpp. Like the CRU operations we update the patients array after we delete the patient
        console.log("--Deleting patient-- Source: delete()")
        $http({
            method:'DELETE',
            url: getUrl() + '/' + currentID,
        }).then(function(response) {
           console.log("--Calling geteAllPatients() in delete()");
            getAllPatients();
        });
        
        /* Test statements for deleting with the local test array
        // Same statement for updating the current position
        //currentPosition = patients.map(function(e) {return e.id;}).indexOf(currentID);
        
        // Splice function here starts an index and deletes 1 object from that position.
        // this deletes the current patient
        //patients.splice(currentPosition, 1);
        
        //debug
        //console.log(patients);
        //delete patients[2];*/
    },
    
};
    


}]);