// Create and name this module
angular.module('prototype.controllers', [])

// Create and name the controller that will be attached to the "home" view. This controller depends on
// The angular scope and ionic modal services. The "Patients" service is a factory created in the
// Services.js file
.controller('PatientsCtrl', function($scope, Patients, $ionicModal, $http) {

    // This logging statement is for debugging
    console.log("<-------------------------Loading Controller -------------------------->");
    
    // Calling the getAllPatient() patients in order to update the patients array in the Patients factory
    console.log("Updating backend array");
    Patients.getAllPatients();
    
    // Create a variable attached to this scope that will hold an array of patients equal to the backend array
    $scope.patients =[];
    
    // Save the promise returned by getPatients() to a variable
    var patientsPromise = Patients.getPatients();
    
    // Basically a function identical to the one located in the Patients factory but saves the result in the scopes patients array
    // Should not be needed since dont want to repeat myself but it was the fastest way to acheive goal. 100% better way to do this
    $scope.getAllPatients = function() {
        patientsPromise.then(function (result) {
           $scope.patients = result.data.data;
            console.log("In controller, updating the front end array in getAllPatients()");
            console.log($scope.patients);
        });
    };
    
    // Another quick fix, use the boolean in the Patients service in order to call the getAllPatients function only when the controller is first loaded. 
    // This is because this method updates the scopes patients array by using an http async call, which is inefficient since we are already calling the server
    // in the backend on the Patients services.
    /*if(Patients.getLoaded() === false)
        {
            console.log("Running first time set up");
            //console.log($scope.loaded);
            $scope.getAllPatients();
            Patients.setLoaded();
            //console.log($scope.loaded);
        }*/
    
    // After the first load of the contoller we use the $watch service to update the scopes patients array. 
    $scope.$watch(function () { return Patients.all()}, function(newVal, oldVal) {
        console.log("Inside watch");
        $scope.patients = Patients.all();
        console.log("--Updating Controller Array in watch--");
        console.log($scope.patients);
    },
                  true
    );
    
    
    //console.log("In controller, updating the controller array. The controller array now holds");
    //$scope.getAllPatients();

    
    //Wrapper function 
    /*$scope.add = function(patient) {
        Patients.add(patient);  
    }*/
    
    // This is a function from the $ionicModal service that we injected. This basically loads a template
    // HTML file when the modal is shown with the "shown()" function.
    $ionicModal.fromTemplateUrl('templates/new-patient.html', function(modal) {
        
        //This saves the "modal" in a variable on the scope so we can refer to it later.
        $scope.patientModal = modal;
    }, {
        
        //Config stuff for the modal
        scope: $scope,
        animation: 'slide-in-up'
    });
    
    // This function is called to create a new patient. I save the patient parameter into a variable 
    // Because it wouldn't work passing to directly to the "Patients.add()" function. Once the function
    // finishes the "important" statements the modal is hidden, and the text input on the modal is
    // cleared.
    $scope.createPatient = function (patient) {
        $scope.patient = patient;
        Patients.add($scope.patient);
        $scope.patientModal.hide();
        patient.Name = "";
        patient.Heart_Rate_Upper="";
        patient.Heart_Rate_Lower="";
        //console.log("In create patient, updating backend array");
        //Patients.getAllPatients();
    }
    
    // This function pulls the modal up
    $scope.newPatient = function() {
        $scope.patientModal.show();
    };
    
    // This function closes the modal
    $scope.closeNewPatient = function() {
        $scope.patientModal.hide();
    };
    
    // This function just changes the "'currentID" in the "Patients" factory in order to change the
    // Information displayed on the patient-info view
    $scope.setID = function(givenID) {
        console.log("Setting current ID to " + givenID);
        Patients.setID(givenID);
        
    };
    
    // Getter function
    $scope.getID = function() {
        console.log(Patients.getID());
    };
    
    $scope.testPrint = function() {
        //Patients.testPrint();
        console.log($scope.patients);
        Patients.update();
    };
    
})

// Create the basic controller for the patient info view
.controller('SpecificCtrl', function($scope, Patients, $state, $http) {
    
    // Save the current patient into a variable so we can reference it in the view
    $scope.currentPatient = Patients.getPatient();
    
    // Testing function
    $scope.change = function() {
        console.log("changing");
        $scope.currentPatient.upperLimit = 12;
        Patients.print();
    };
    
    // This is the function that gets called whenever the user updates a patients
    // paramters. Testing showed that not every phone can submit on input when they
    // click "go" or "enter", as a result we need a dedicated submit button.
    // Because it is a dedicated submit button, we need to pass every updated
    // parameter to the "Patients" factory and deal with the management on the
    // factory side. So in essense this is a jumped up wrapper function
    $scope.update = function(updatedHeartUpper, updatedHeartLower) {
                
                // Just some debugging stuff
                console.log("New upper limit is  " + updatedHeartUpper);
                console.log("New lower limit is  " + updatedHeartLower);
                
                // Store the updated parameters into variables attached to the scope
                // TODO: Only assign when not undefined
                $scope.updatedOne = updatedHeartUpper;
                $scope.updatedTwo = updatedHeartLower;
                
        
                // The call to the main function where the business logic is handled
                Patients.update($scope.updatedOne,$scope.updatedTwo);
                
                // Once the main update function has been called use the convenience method to switch back to the home screen
                $state.go('home');
                
    };
    
    // Wrapper function to delete a patient
    $scope.delete = function() {
        Patients.delete();
    };
    
    /*$scope.getID = function (){
        console.log(Patients.getID());
    }*/
    
    /*
    $scope.getPatient = function () {
       $scope.currentPatient = Patients.getPatient(); console.log($scope.currentPatient);
    }*/
    
    
    
})